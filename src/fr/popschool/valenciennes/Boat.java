package fr.popschool.valenciennes;

public class Boat {

    private Integer size;

    private Direction direction;

    private Position position;

    private Integer nbHits;

    public Boat(Integer size, Direction direction, Position position, Integer nbHits) {
        this.size = size;
        this.direction = direction;
        this.position = position;
        this.nbHits = nbHits;
    }

    public Boat() {
    }

    public Boat(Integer size, Direction direction, Position position) {
        this.size = size;
        this.direction = direction;
        this.position = position;
        this.nbHits = 0;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Integer getNbHits() {
        return nbHits;
    }

    public void setNbHits(Integer nbHits) {
        this.nbHits = nbHits;
    }
}
