package fr.popschool.valenciennes;

public class Cell {

    private CaseState caseState;

    private Boat boat = null;

    public Cell(CaseState caseState) {
        this.caseState = caseState;
    }

    public Cell(CaseState caseState, Boat boat) {
        this.caseState = caseState;
        this.boat = boat;
    }

    public Cell() {
    }

    public CaseState getCaseState() {
        return caseState;
    }

    public void setCaseState(CaseState caseState) {
        this.caseState = caseState;
    }

    public Boat getBoat() {
        return boat;
    }

    public void setBoat(Boat boat) {
        this.boat = boat;
    }
}
