package fr.popschool.valenciennes;

import java.util.LinkedList;
import java.util.Scanner;

public class Board {

    private LinkedList<LinkedList<Cell>> board;

    private Integer size;

    public Board(LinkedList<LinkedList<Cell>> board, Integer size) {
        this.board = board;
        this.size = size;
    }

    public Board() {
        this.setSize(10);
        this.board = new LinkedList<>();
        this.initBoard();
    }


    private  void initBoard(){
        for (int i = 0; i < this.getSize(); i++) {
            board.add(new LinkedList<>());
            for (int j = 0; j < this.getSize(); j++) {
                board.get(i).add(new Cell(CaseState.X));
            }
        }
    }

    public LinkedList<LinkedList<Cell>> getBoard() {
        return board;
    }

    public void setBoard(LinkedList<LinkedList<Cell>> board) {
        this.board = board;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    @Override
    public String toString() {
        var ref = new Object() {
            String outPut = "";
        };

        this.getBoard().forEach((line) -> {
            ref.outPut = ref.outPut.concat("|");
            line.forEach((cell) -> {
                ref.outPut = ref.outPut.concat("X");
   //            ref.outPut = ref.outPut.concat(cell.getCaseState().name());
                ref.outPut = ref.outPut.concat("|");
            } );
            ref.outPut = ref.outPut.concat("\n");
        });

        /*String outPut2 = "";
        for (int i = 0; i < this.getSize(); i++) {
            outPut2 += "|";
            for (int j = 0; j < this.getSize(); j++) {
                outPut2 += this.getBoard().get(i).get(j).getCaseState().name();
                outPut2 += "|";
            }
            outPut2 += "\n";
        }
*/
        return ref.outPut;
    }
}
