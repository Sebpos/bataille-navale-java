package fr.popschool.valenciennes;

import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;

public class Game {

    private Board board;

    private LinkedList<Boat> boats;

    public Game(Board board, LinkedList<Boat> boats) {
        this.board = board;
        this.boats = boats;
    }

    public Game() {
        this.board = new Board();
        this.boats = new LinkedList<>();
        for (int i = 0; i < 4; i++) {
            Random random = new Random();
            Boat boat = this.placeBoat(new Boat((random.nextInt(2) + 2),(random.nextInt(1) == 0 ? Direction.H : Direction.V),(new Position(random.nextInt(this.getBoard().getSize()),random.nextInt(this.getBoard().getSize())))));
            if(boat != null){
                this.boats.add(boat);
            }
        }
    }

    private Boat placeBoat(Boat boat) {
        if(this.checkBoat(boat)){
            for (int i = 0; i < boat.getSize(); i++) {
                if(boat.getDirection() == Direction.H){
                    this.getBoard().getBoard().get(boat.getPosition().getX() + i).set(boat.getPosition().getY(), new Cell(CaseState.B, boat));
                }else{
                    this.getBoard().getBoard().get(boat.getPosition().getX()).set(boat.getPosition().getY() + i, new Cell(CaseState.B, boat));
                }
            }
            return boat;
        }else{
            return null;
        }
    }

    private Boolean checkBoat(Boat boat){
        Boolean isOk = true;
        for (int i = 0; i < boat.getSize(); i++) {
            if(boat.getDirection() == Direction.H){
                if(boat.getPosition().getX() + i < this.getBoard().getSize()){
                    if(this.getBoard().getBoard().get(boat.getPosition().getX() + i).get(boat.getPosition().getY()).getCaseState() != CaseState.X){
                        isOk = false;
                    }
                }else {
                    isOk = false;
                }
            }else{
                if(boat.getPosition().getY() + i < this.getBoard().getSize()){
                    if(this.getBoard().getBoard().get(boat.getPosition().getX()).get(boat.getPosition().getY() + i).getCaseState() != CaseState.X){
                        isOk = false;
                    }
                }else {
                    isOk = false;
                }

            }
        }
        return isOk;
    }

    public void play(){
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println(this.getBoard());
            System.out.println("X : ");
            Integer x = scanner.nextInt();
            System.out.println("Y : ");
            Integer y = scanner.nextInt();
            this.shoot(new Position(x,y));

        }while(!this.gameIsWin());
    }

    private void shoot(Position position) {
        if(this.getBoard().getBoard().get(position.getX()).get(position.getY()).getCaseState() == CaseState.B){
            this.getBoard().getBoard().get(position.getX()).get(position.getY()).setCaseState(CaseState.T);
            this.getBoard().getBoard().get(position.getX()).get(position.getY()).getBoat().setNbHits(this.getBoard().getBoard().get(position.getX()).get(position.getY()).getBoat().getNbHits() + 1);
            System.out.println("Bien joué vous avez touché un bateau !!!");
        }else {
            System.out.println("Dommage");
        }
    }

    private boolean gameIsWin() {
        int totalSize = 0, totalHits = 0;
        for (Boat bo :
                this.getBoats()) {
            totalSize += bo.getSize();
            totalHits += bo.getNbHits();
        }

        return totalHits == totalSize;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public LinkedList<Boat> getBoats() {
        return boats;
    }

    public void setBoats(LinkedList<Boat> boats) {
        this.boats = boats;
    }

    @Override
    public String toString() {
        return this.getBoard().toString();
    }
}
